Скрипт для отправки списка ssh команд на несколько ip-адресов.

Для работы скрипта необходимо создать два файла c точным названием:
1) "IPs.txt"
2) "Cmd.txt"
Файл "IPs.txt" необходимо заполнить ip-адресами устройств, на которые будут отправляться команды.
ВАЖНО!!! Все ip должны логиниться под одним и тем же логином и паролем! (Логин, пароль указывается при запуске скрипта.)
ВАЖНО!!! При заполнении, после каждого ip должен быть перенос (ENTER), без лишних символов, иначе скрипт не будет работать.
Пример:
127.0.0.1
0.0.0.0

Файл "Cmd.txt" необходимо заполнить списком команд, которые Вы хотите отправить.
ВАЖНО!!! Каждая новая команда должна начинаться с новой строки(ENTER)
ВАЖНО!!! Если команда требует подтверждения - скрипт не отрабатывает (пожалуйста учитывайте это при заполнении файла команд)
Пример:
mkdir test
cd test
touch text.txt

Принцип работы:
1)Открываем скрипт
2)Вводим логин и пароль
Для каждого ip, создается отдельный файл логов в папке log

Последняя версия скрипта:
https://gitlab.com/mrviduus/ssh_cmd
